// rgb to hex converters
// https://stackoverflow.com/questions/5623838/rgb-to-hex-and-hex-to-rgb
// modified to take array
const rgbToHex = (rgb) => '#' + rgb.map(x => {
    const hex = x.toString(16)
    return hex.length === 1 ? '0' + hex : hex
  }).join('')

const hexToRgb = hex =>
    hex.replace(/^#?([a-f\d])([a-f\d])([a-f\d])$/i
                ,(m, r, g, b) => '#' + r + r + g + g + b + b)
    .substring(1).match(/.{2}/g)
    .map(x => parseInt(x, 16))

var app = new Vue({ 
    el: '#container',
    data: {
        primary: '#43090A',
        accent: '#562425',
        accentVariant: "lighter",
        header: '#000000',
        menuPrimary: false,
        headerFont: '#FFFFFF',
        menuButtonFont: '#FFFFFF',
        contentFont: '#2c3e50',
        searchBorder: '#ecf0f1',
        contentBg: '#FFFFFF',
        menuBorder: '#FFFFFF',
        footerHeader: true
    },
    computed: {
        // Footer matches header by default but can be reset
        footer() {
            if (this.footerHeader) {
                return this.header;
            }
            else {
                return this.primary;
            }
        },
        // Menu border color is settable normally unless it's being tagged to primary by menuPrimary
        menuBorderCustom: {
            get: function() {
                if (this.menuPrimary) {
                    this.menuBorder = this.primary;
                    return this.primary;
                }
                else {
                    return this.menuBorder;
                }
            },
            set: function(newVal) {
                this.menuBorder = newVal;
            }
            
        },
        primaryDark() {
            return(this.tinter("darker"));
        },
        customStyle() {
            return {'--primary-color': this.primary,
                    '--primary-dark': this.primaryDark, 
                    '--accent-color': this.accent,
                    '--header-bg': this.header,
                    '--menu-border': this.menuBorder,
                    '--header-font': this.headerFont,
                    '--search-border': this.searchBorder,
                    '--content-bg': this.contentBg,
                    '--menu-button-font' : this.menuButtonFont,
                    '--footer-bg': this.footer};
        }
    },
    methods: {
        // Reset to the default PARC colors
        defaultColors: function(event) {
            this.menuPrimary = false;
            this.primary = '#43090A';
            this.accent = '#562425';
            this.header = '#000000';
            this.headerFont = '#FFFFFF';
            this.menuButtonFont = '#FFFFFF';
            this.contentFont = '#2c3e50';
            this.searchBorder = '#ecf0f1';
            this.contentBg = '#FFFFFF';
            this.menuBorder = '#FFFFFF';
        },
        tinter: function(tint) {
            value = this.primary;
            if (tint == "lighter") {
                value = hexToRgb(this.primary).map(x => {return Math.floor(x*1.33,0)})
            }
            else {
                value = hexToRgb(this.primary).map(x => {return Math.floor(x*0.66,0)});
            }
            // Check if value is valid - if too long (most common), set to white
            value = rgbToHex(value);
            if (value.length > 7) {
                value = "#FFFFFF"
            }
            return value;
            
        },
        exportCSS: function(event) {
            const generatedCSS = ".menuSection {\n    background-color: " + this.primary + ";\n}\n\n"+ 
            ".btn-success, .btn-warning {\n    background-color: " + this.primary + ";\n    border-color: "+ this.primaryDark + "\n}\n\n" +
            ".btn-success:hover, .btn-warning:hover {\n    background-color: " + this.accent + ";\n}\n\n" +
            ".navbar-default, footer .footer-below {\n    background-color: " + this.header + ";\n}\n\n" +
            ".navbar-default .navbar-brand {\n    color: " + this.headerFont + ";\n}\n\n" +
            "footer .footer-below {\n    background-color: " + this.footer + ";\n}"

            // Enforces 4 spaces instead of tabs, sorry
            navigator.clipboard.writeText(generatedCSS);
        }
    }
});


